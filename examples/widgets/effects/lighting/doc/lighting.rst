Lighting Example
================

This example demonstrates a PySide6 application that creates a dynamic scene with lighting and
shadow effects using `QGraphicsView` and `QGraphicsScene`. It features animated light sources and
graphical items with drop shadows that respond to the light, showcasing advanced rendering and
animation techniques.

.. image:: lighting.webp
    :width: 400
    :alt: lighting screenshot
