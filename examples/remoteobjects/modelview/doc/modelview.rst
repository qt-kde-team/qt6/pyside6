Model-View Server Example
=========================

Developing a simple server program that displays and makes changes to a
QTreeView which is made available on a Remote Objects network. It
corresponds to the Qt example `Model-View Server Example
<https://doc.qt.io/qt-6/qtremoteobjects-modelviewserver-example.html>`_.

.. image:: modelview.png
    :align: center
    :alt: modelview screenshot
    :width: 400
