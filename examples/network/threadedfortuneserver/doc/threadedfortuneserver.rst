Threaded Fortune Server Example
===============================

The Threaded Fortune Server example shows how to create a server for a simple
network service that uses threads to handle requests from different clients.
It corresponds to the Qt example `Threaded Fortune Server Example
<https://doc.qt.io/qt-6/qtnetwork-threadedfortuneserver-example.html>`_.

.. image:: threadedfortuneserver.png
    :align: center
    :alt: threadedfortuneserver screenshot
    :width: 400
