Fortune Client Example
======================

Demonstrates how to create a client for a network service. It corresponds to the
Qt example `Fortune Client Example
<https://doc.qt.io/qt-6/qtnetwork-fortuneclient-example.html>`_.

.. image:: fortuneclient.png
    :align: center
    :alt: fortuneclient screenshot
    :width: 400
