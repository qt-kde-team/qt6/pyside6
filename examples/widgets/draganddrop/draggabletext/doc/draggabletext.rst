Draggable Text Example
======================

Illustrates how to drag and drop text between widgets.

.. image:: draggabletext.png
    :width: 400
    :alt: draggabletext screenshot
