System Tray Icon Example
========================

The System Tray Icon example shows how to add an icon with a menu and popup
messages to a desktop environment's system tray. It demonstrates the analogous
Qt example `System Tray Icon Example
<https://doc.qt.io/qt-6/qtwidgets-desktop-systray-example.html>`_.

.. image:: systray.png
   :align: center
