MDI Example
===========

The MDI example shows how to implement a Multiple Document Interface using
Qt's `QMdiArea` class.

.. image:: mdi.png
    :width: 400
    :alt: mdi screenshot
