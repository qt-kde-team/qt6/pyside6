D-Bus Ping Pong Example
=======================

Demonstrates a simple message system using D-Bus.

Ping Pong is a command-line example that demonstrates the basics of Qt D-Bus.
A message is sent to another application and there is a confirmation of the
message.
