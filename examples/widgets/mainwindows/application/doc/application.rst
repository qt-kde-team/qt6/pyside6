Application Example
===================

This application is a simple text editor built using PySide6, demonstrating
how to create a modern GUI application with a menu bar, toolbars, and a status
bar. It supports basic file operations such as creating, opening, saving, and
editing text files. It demonstrates the analogous Qt example `Application Example
<https://doc.qt.io/qt-6.2/qtwidgets-mainwindows-application-example.html>`_.

.. image:: application.png
    :width: 400
    :alt: application screenshot
