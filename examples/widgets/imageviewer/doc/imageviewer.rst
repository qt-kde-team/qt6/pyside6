Image Viewer Example
====================

This example demonstrates an image viewer application built using PySide6,
featuring functionalities such as opening, saving, printing, copying, pasting,
and zooming images within a scrollable area. This example demonstrates the
analogous Qt example `Image Viewer Example
<https://doc.qt.io/qt-6.2/qtwidgets-widgets-imageviewer-example.html>`_.

.. image:: imageviewer.webp
    :width: 400
    :alt: imageviewer screenshot
